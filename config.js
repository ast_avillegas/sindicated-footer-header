module.exports = {
  baseURL: 'https://dev41-lora-loreal.demandware.net/',
  SFCC: 'on/demandware.store/',
  site: 'Sites-hair-us-Site/',
  locale: 'en_US/',
  headerEndpoint: 'HomeInclude-Header',
  footerEndpoint: 'HomeInclude-Footer',
  assetsEndpoint: 'HomeInclude-Assets'
};