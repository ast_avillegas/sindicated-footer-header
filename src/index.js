'use strict';
//elements to render
import {_embed} from "./embedElements";
import {functionSyncIterator, getData} from "./services/renderElements"

export default class Sindicated {

    //fetch elements defined in _embed and add in page
    getElements() {
        let fnArray = [];
        let embed = Object.assign({}, _embed)
        for (let index = 0; index < Object.keys(embed).length; index++) {
            let section = Object.values(embed)[index];
            let fn = getData.bind(this, section);
            fnArray.push(fn);
        }
        functionSyncIterator(fnArray, 0);
    }
}