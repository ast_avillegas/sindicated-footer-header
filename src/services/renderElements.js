"use strict";
///Recursive function to iterate synchronically through async functions
export function functionSyncIterator(fnArray, idx) {
    if (!fnArray[idx]) return;
    let currentFunction = fnArray[idx];
    let newIdx = idx + 1;
    currentFunction()
      .then(r => functionSyncIterator(fnArray, newIdx))
      .then(s => {
        document.querySelector('.c-simple-signup__form').remove();
        document.querySelector('.l-header__search-cta').remove();
      })
  }

//fetch Elements
export function getData(section) {
    return fetch(section.URL).then((response) => {
            return response.text();
        })
        .then(response => {
            if (response) {
                let nodeToReplace = section.selector && document.querySelector(section.selector);
                let nodeList = new DOMParser().parseFromString(response, "text/html").querySelectorAll(section.selector);
                let fnArray = [];
                for (let index = 0; index < nodeList.length; index++) {
                    const node = nodeList[index];
                    let fn = createElement.bind(this, node, nodeToReplace, section);
                    fnArray.push(fn);
                }
                functionSyncIterator(fnArray, 0);
            }
        })
        .catch((err) => {
            console.log('Fetch Error', err);
        });
}

//evaluate scripts and add them to document
function createElement(node, nodeToReplace, section) {
    return new Promise(function (resolve, reject) {
        if (nodeToReplace && nodeToReplace.length === 1) {
            nodeToReplace.rplaceWith(node);
            resolve();
        } else {
            if (node.nodeName === 'SCRIPT') {
                var scriptNode = document.createElement("script");
                if (node.src) {
                    scriptNode.src = node.src;
                    scriptNode.async = false;
                    scriptNode.onload = function (e) {
                        resolve();
                    }
                    node = scriptNode;
                    //overwrite configuration to avoid cors issues
                    if (node.src.includes('dist/javascripts/main.js')) {
                        System.meta['*.js'] = {
                            scriptLoad: true
                        }
                    }
                    document.body[section.appendType](node);
                } else {
                    var inlineScript = node.text;
                    scriptNode.text = inlineScript;
                    node = scriptNode;
                    document.body[section.appendType](node);
                    resolve();
                }
            } else {
                document.body[section.appendType](node);
                resolve();
            }
        }
    });
}