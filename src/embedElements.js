"use strict";
import {
    baseURL,
    SFCC,
    site,
    locale,
    headerEndpoint,
    footerEndpoint,
    assetsEndpoint
} from "../config";

module.exports = {
    //elements to render
    _embed: {
        header: {
            URL: baseURL + SFCC + site + locale + headerEndpoint,
            selector: 'header',
            appendType: 'prepend'
        },
        footer: {
            URL: baseURL + SFCC + site + locale + footerEndpoint,
            selector: 'footer',
            appendType: 'append'
        },
        script: {
            URL: baseURL + SFCC + site + locale + assetsEndpoint,
            selector: 'link, script',
            appendType: 'append'
        }
    }
}